# Presentable

When I beleive I have something worth sharing, i'll add it here. The plan is to have:
    * Complete 'modules' that explain what I'm doing
    * worked out examples and demonstrations

The executable binder of this repo is here:
* [![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/jumson%2Fpinode_notes/master)

That will launch the entire operating environment and the Jupyter notebook files for you to mess with, exxecute, and basically wreak havok with.

It wont (shouldnt) have any effect on my actual work. All my most updated work and scratch notes, etc. is here.
