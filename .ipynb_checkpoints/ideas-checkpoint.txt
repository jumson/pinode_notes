interactive graphs, etc: https://github.com/matplotlib/jupyter-matplotlib
https://github.com/markusschanta/awesome-jupyter

Realtime collaboration for JupyterLab using Google Drive
https://github.com/jupyterlab/jupyterlab-google-drive


https://github.com/jupyterlab/jupyterlab-toc
A Table of Contents extension for JupyterLab. This auto-generates a table of contents in the left area when you have a notebook or markdown document open. 

https://github.com/jupyterlab/jupyterlab-git
GIT IT!