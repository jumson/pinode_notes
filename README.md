# pinode_notes

This is where I'll move all my documentation and notes -- using Jupyter executable notebooks.
**Public** accessible!

Eventually, this page will be sort of a Table of Contents, with clickable links to executable binders.


[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/jumson%2Fpinode_notes/master)
Clicking the link above will launch my entire operating environment and the Jupyter notebook files for you to mess with, exxecute, and basically wreak havok with.
It wont (shouldnt) have any effect on my actual work. All my most updated work and scratch notes, etc. is here.
At some point, my scratch work will be consolidated, and the more 'finished' demos will be linked specifically.